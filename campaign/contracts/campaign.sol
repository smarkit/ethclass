pragma solidity ^0.4.17;

contract CampaignFactory {
    address public manager;
    address[] public campaigns;

    constructor() public {
        manager = msg.sender;
    }

    function deployCampaign(uint min) public {
        address newCampaign = new Campaign(min, msg.sender);
        campaigns.push(newCampaign);
    }

    function getCampaigns() public view returns (address[]) {
        return campaigns;
    }
}

contract Campaign {
    struct Request {
        uint value;
        bool completed;
        address recipient;
        string description;
        uint votesCount;
        mapping(address => bool) voters;
    }

    address public manager;
    uint public minEther;
    Request[] public requests;
    mapping(address => bool) public contributors;
    uint totalContributors;

    modifier restricted(){
        require(msg.sender == manager);
        _;
    }

    modifier isContributor(){
        require(contributors[msg.sender]);
        _;
    }
    
    constructor(uint min, address man) public{
        manager = man;
        minEther = min;
    }
    
    function contribute() public payable {
        require(msg.value > minEther);
        contributors[msg.sender] = true;
        totalContributors++;
    }

    function createRequest(string desc, uint value, address recipient) public restricted  {
        Request memory newRequest = Request({
            value: value,
            completed: false,
            recipient: recipient,
            description: desc,
            votesCount: 0
        });
        requests.push(newRequest);
    }

    function approveRequest(uint requestIndex) public isContributor  {
        Request storage request = requests[requestIndex];
        require(!request.voters[msg.sender]);
        request.votesCount++;
        request.voters[msg.sender] = true;
    }

    function finalizeRequest(uint requestIndex) public restricted  {
        Request storage request = requests[requestIndex];
        require(!request.completed);
        require(request.votesCount > (totalContributors / 2));
        request.recipient.transfer(request.value);
        request.completed = true;
    }
}