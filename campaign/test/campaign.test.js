
const chai = require("chai");
const assert = chai.assert;

const Web3 = require("web3");
const mocha = require("mocha");
const ganache = require("ganache-cli");
const contracts = require("../cmd/contracts.js");

// create web3 providing using ganache.
const web3 = new Web3(ganache.provider());

const Campaign = contracts.getContract("campaign");
const CampaignFactory  = contracts.getContract("campaignfactory");

let accts;
let factoryContract;
let campaignAddresses, campaignAddress, campaignContract;

// load ganache node and retrieve accounts. 
beforeEach(async() => {
    // get all acounts
    accts = await web3.eth.getAccounts()

    // get a random acct and deploy Inbox contract.
    factoryContract = await (new web3.eth.Contract(JSON.parse(CampaignFactory.interface))
        .deploy({ data: CampaignFactory.bytecode })
        .send({ from: accts[0], gas: "1000000" }))


    await factoryContract.methods.deployCampaign("100").send({
        from: accts[0],
        gas: "1000000",
    });

    campaignAddresses = await factoryContract.methods.getCampaigns().call({from: accts[0]});
    campaignAddress = campaignAddresses[0];
    campaignContract = await (new web3.eth.Contract(JSON.parse(Campaign.interface), campaignAddress));
});

// test basic message setup.
describe("Campaigns", () => {
    it("Should have deployed a campaign factory and campaign", () => {
        assert.isDefined(factoryContract);
        assert.isDefined(campaignAddresses);
        assert.isDefined(campaignAddress);
        assert.isDefined(campaignContract);
    });

    it("marks caller as the campaign manager", async () => {
        const manager = await campaignContract.methods.manager().call();
        assert.ok(manager);
        assert.equal(accts[0], manager);
    });

    it("can become a contributor to campaign", async () => {
        await campaignContract.methods.contribute().send({
            value: "200",
            from: accts[1],
        });

        const isContributors = await campaignContract.methods.contributors(accts[1]).call();
        assert(isContributors);
    });

    it("requires minimum contribution", async () => {
        try {
            await campaignContract.methods.contribute().send({
                value: "20",
                from: accts[1],
            });
        }catch(e){
            assert.isDefined(e);
        }
    });

    it("manager can create request", async () => {
        await campaignContract.methods.createRequest("Bottle gas", "40", accts[2]).send({
            from: accts[0],
            gas: "1000000",
        });

        const request = await campaignContract.methods.requests(0).call();
        assert.ok(request);
        assert.equal(request.recipient, accts[2]);
    });


    it("can create and finalize camaign request", async () => {
        const acct1LastBalance = await web3.eth.getBalance(accts[1]);

        await campaignContract.methods.contribute().send({
            value: web3.utils.toWei("10", "ether"),
            from: accts[1],
        });

        const isContributors = await campaignContract.methods.contributors(accts[1]).call();
        assert(isContributors);

        await campaignContract.methods.createRequest("Bottle gas", web3.utils.toWei("5", "ether"), accts[6]).send({
            from: accts[0],
            gas: "1000000",
        });

        let request = await campaignContract.methods.requests(0).call();
        assert.ok(request);
        assert.equal(request.recipient, accts[6]);

        await campaignContract.methods.approveRequest(0).send({
            from: accts[1],
            gas: "1000000",
        });

        request = await campaignContract.methods.requests(0).call();
        assert(request.votesCount == 1);

        const acct6LastBalance = await web3.eth.getBalance(accts[6]);

        assert.equal(acct1LastBalance, 99998320419999999800);
        assert.equal(acct6LastBalance, 100000000000000000000);

        await campaignContract.methods.finalizeRequest(0).send({
            from: accts[0],
            gas: "1000000",
        });

        const acct6NewBalance = await web3.eth.getBalance(accts[6]);
        assert.equal(acct6NewBalance, 105000000000000000000);
    });
});