const Web3 = require("web3");

// get all acounts
const deploy = async function(provider, contract) {
    const web3 = new Web3(provider);
    const { interface, bytecode } = contract;

    console.log("About Deploy contract");

    console.log("Retrieving accounts to deploy contract");
    const accts = await web3.eth.getAccounts();
    console.log("Deploy using acct: ", accts[0]);

    const newContract = await (new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: bytecode })
        .send({ from: accts[0], gas: "1000000" }));

    console.log("Contract deployed to: ", newContract.options.address);
    console.log("Contract Interface: ", interface);
    return newContract;
}


module.exports = {
    Deploy: deploy,
}