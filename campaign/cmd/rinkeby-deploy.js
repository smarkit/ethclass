const contracts = require("./contracts");
const deployer = require("./deploy");

const Web3 = require("web3");
const TruffleWallet = require("truffle-hdwallet-provider");

// create web3 providing using truffle hdwallet.
const mn = 'butter ice region child increase tornado gloom ostrich fringe wear act oil';
const provider = new TruffleWallet(mn, "https://rinkeby.infura.io/7MTlJ95S18oaFPlRBvmI");

const campaignFactory = contracts.getContract("campaignfactory");

deployer.Deploy(provider, campaignFactory);