const fs = require("fs-extra");
const path = require("path");
const solc = require("solc");

// build directory.
const buildDir = path.resolve(__dirname, "../build");

// contracts directory.
const constractsDir = path.resolve(__dirname, "../contracts");

// read all paths in directory sync. 
const contractFiles = fs.readdirSync(constractsDir);

// compile contracts from files.
let sources = {};
contractFiles.forEach(element => {
    let source = fs.readFileSync(path.join(constractsDir, element), "utf8");
    sources[element] = source;
});

// only works with files within same directory level.
const compiled = solc.compile({sources: sources}, 1, function(imported){
    let content = null;
    try {
        content = fs.readFileSync(path.join(contractsDir, imported))
    }catch(e){
        return { error: e};
    }
    return {contents: content};
});

// clean up current build directory.
fs.removeSync(buildDir);
fs.mkdirSync(buildDir);

// store contracts abi and interface into build directory.
for (var contractName in compiled.contracts){
    const contract = contractName.split(":")[1];
    const contractFileName = [contract.toLowerCase(), "contract", "json"].join(".");
    const contractObj = compiled.contracts[contractName];
    fs.writeFileSync(path.join(buildDir,contractFileName), JSON.stringify(contractObj));
}