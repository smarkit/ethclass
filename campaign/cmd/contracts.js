const fs = require("fs");
const path = require("path");
const solc = require("solc");

// build directory.
const buildDir = path.resolve(__dirname, "../build");

// compile contracts from files.
let sources = {};

fs.readdirSync(buildDir).forEach(element => {
    const contractName = element.split(".")[0];
    const source = fs.readFileSync(path.join(buildDir, element), "utf8");
    sources[contractName] = JSON.parse(source);
});

module.exports = {
    getContract : function(name){
        return sources[name];
    },
}