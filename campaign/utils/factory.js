import web3 from "./metamask-web3";
import * as Factory from "../cmd/contracts";

var campaignFactory = Factory.getContract("campaignfactory");

export default new web3.eth.Contract(
    JSON.parse(campaignFactory.interface),
    "0x8B09C7b6386365533C9270344942C76D7b2f20Df",
);