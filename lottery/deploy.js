const Web3 = require("web3");
const mocha = require("mocha");
const contracts = require("./compile.js");
const TruffleWallet = require("truffle-hdwallet-provider");

const mn = 'butter ice region child increase tornado gloom ostrich fringe wear act oil';

// create web3 providing using truffle hdwallet.
const provider = new TruffleWallet(mn, "https://rinkeby.infura.io/7MTlJ95S18oaFPlRBvmI")
const web3 = new Web3(provider);
const { interface, bytecode } = contracts.get("Lottery");

// get all acounts
const deploy = async function() {
    console.log("About Deploy contract");
    const accts = await web3.eth.getAccounts();
    console.log("Deploy using acct: ", accts[0]);

    const contract = await (new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: bytecode })
        .send({ from: accts[0], gas: "1000000" }));

    console.log("Contract deployed to: ", contract.options.address);
    console.log("Contract Interface: ", interface);
}

deploy();