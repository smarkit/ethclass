const fs = require("fs");
const path = require("path");
const solc = require("solc");

// contracts directory.
const constractsDir = path.resolve(__dirname, "contracts");

// read all paths in directory sync. 
const contractFiles = fs.readdirSync(constractsDir);

// compile contracts from files.
let compiled = [];
contractFiles.forEach(element => {
    let source = fs.readFileSync(path.join(constractsDir, element), "utf8");
    compiled.push(solc.compile(source, contractFiles.length));
});

// ContractRepo decorates a provided contract object 
// with get method.
class ContractRepo {
    constructor(contracts) {
        this.contracts = contracts;
    }

    get(contract) {
        let item;
        const name = ":" + contract;
        this.contracts.forEach(c => {
            if (c.contracts[name] && !item) {
                item = c.contracts[name];
                return
            }
        });
        return item;
    }

    src() {
        return this.contracts;
    }
}

// export out a function that exposes compiled data.
module.exports = new ContractRepo(compiled);