const chai = require("chai");
const assert = chai.assert;

const Web3 = require("web3");
const mocha = require("mocha");
const ganache = require("ganache-cli");
const contracts = require("../compile.js");

// create web3 providing using ganache.
const web3 = new Web3(ganache.provider());
const { interface, bytecode } = contracts.get("Lottery");

let accts;
let contract;

// load ganache node and retrieve accounts. 
beforeEach(async() => {
    // get all acounts
    accts = await web3.eth.getAccounts()

    // get a random acct and deploy Inbox contract.
    contract = await (new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: bytecode })
        .send({ from: accts[0], gas: "1000000" }))
});

// test basic message setup.
describe("Lottery", () => {
    it("Should have deployed contract", () => {
        assert.isDefined(contract);
        assert.isDefined(contract.options);
        assert.isDefined(contract.options.address);
    });

    it("allows one account to enter", async () => {
        await contract.methods.enter().send({
            from: accts[1],
            value: web3.utils.toWei("0.02", "ether"),
        });

        const players = await contract.methods.getPlayers().call({from: accts[0]});
        assert.equal(accts[1], players[0]);
        assert.equal(1, players.length);
    });

    it("win lottery with one account", async () => {
        const initialBalance = await web3.eth.getBalance(accts[1]);

        await contract.methods.enter().send({
            from: accts[1],
            value: web3.utils.toWei("2", "ether"),
        });

        const newBalance = await web3.eth.getBalance(accts[1]);
        assert.notEqual(newBalance, initialBalance);

        let players = await contract.methods.getPlayers().call({from: accts[0]});
        assert.equal(accts[1], players[0]);
        assert.equal(1, players.length);

        try {
            await contract.methods.pickWinner().send({from: accts[0]});
        }catch(e){
            assert.fail(e, null, "error should not have occured");
        }

        players = await contract.methods.getPlayers().call({from: accts[0]});
        assert.equal(0, players.length);

        const finalBalance = await web3.eth.getBalance(accts[1]);
        assert.isOk(finalBalance > newBalance);
    });

    it("only manager can call pickwinner", async () => {
        try {
            await contract.methods.pickWinner().send({
                from: accts[1],
                value: web3.utils.toWei("0.002", "ether"),
            });
            assert.fail();
        }catch(e){
            assert.isDefined(e);
        }
    });

    it("require minimum of 0.1 ether", async () => {
        try {
            await contract.methods.enter().send({
                from: accts[1],
                value: web3.utils.toWei("0.002", "ether"),
            });
            assert(false);
        }catch(e){
            assert.isDefined(e);
        }

        const players = await contract.methods.getPlayers().call({from: accts[0]});
        assert.notEqual(accts[1], players[0]);
        assert.equal(0, players.length);
    });

    it("allows many account to enter", async () => {
        for(let i = 1; i < accts.length; i++){
            await contract.methods.enter().send({
                from: accts[i],
                value: web3.utils.toWei("0.02", "ether"),
            });

            let ep = await contract.methods.getPlayers().call({from: accts[0]});
            assert.equal(accts[i], ep[i-1]);
        }

        const players = await contract.methods.getPlayers().call({from: accts[0]});
        assert.equal(accts.length-1, players.length);
    });
});