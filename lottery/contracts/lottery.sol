pragma solidity ^0.4.17;

contract Lottery {
    address public manager;
    address[] public players; 

    constructor() public {
        manager = msg.sender;
    }

    function enter() public payable {
        require(msg.value > .01 ether);
        require(msg.sender != manager);
        players.push(msg.sender);
    }
    
    function getPlayers() public view returns (address[]) {
        return players;
    }

    function enteredPlayers() public restricted view returns (uint) {
        return players.length;
    }
    
    function rand() private view returns (uint) {
        return uint(keccak256(abi.encode(block.difficulty, block.timestamp, players)));
    }
    
    function pickWinner() public restricted {
        uint index = (rand() & players.length);
        if(index >= players.length) index -= 1;
        address winner = players[index];
        winner.transfer(address(this).balance);
        players = new address[](0);
    }

    modifier restricted() {
        require(msg.sender == manager);
        _;
    }
}