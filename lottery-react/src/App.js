import React, { Component } from 'react';
import logo from './logo.svg';
import web3 from "./web3";
import './App.css';
import lottery from "./lottery";


class App extends Component {
  state = {manager:"", players:[], value:"", balance:"", message:""};
  
  async componentDidMount(){
    const manager = await lottery.methods.manager().call();
    const players = await lottery.methods.getPlayers().call();
    const balance = await web3.eth.getBalance(lottery.options.address);
    this.setState({ manager: manager, players: players, balance: balance});
  }

  onSubmit = async (e) => {
    e.preventDefault();

    const accts = await web3.eth.getAccounts();
    console.log(`Accounts#${accts.length}: ${accts}`);
    this.setState({message: "Waiting on transaction success...."});
    try {
      await lottery.methods.enter().send({from: accts[0], value: web3.utils.toWei(this.state.value, "ether")});
    }catch(e){
      this.setState({message: "Transaction failed!...."});
      console.log("Error: ", e);
      return
    }

    this.setState({message: "Transaction successfull!...."});
  }

  onPickWinner = async (e) => {
    e.preventDefault();

    const accts = await web3.eth.getAccounts();
    console.log(`Accounts#${accts.length}: ${accts}`);

    this.setState({message: "Waiting on winner...."});

    try {
      await lottery.methods.pickWinner().send({from: accts[0]});
    }catch(e){
      this.setState({message: "Winner failed to be selected!...."});
      console.log("Error: ", e);
      return
    }

    this.setState({message: "Winner Selected!...."});
  }

  render() {
    return (
      <div>
        <h2>Lottery Contract</h2>
        <p>This contract is managed by {this.state.manager}.</p>
        <p>There are currently {this.state.players.length} people entered
          competing to win {web3.utils.fromWei(this.state.balance, "ether")} ether;
        </p>
        <hr/>
        <form onSubmit={this.onSubmit}>
          <h4>Want to try your luck?</h4>
          <div>
            <label>Amount of Ether to enter:</label>
            <input type="text" value={this.state.value} onChange={e => this.setState({value: e.target.value})} />
          </div>
          <button>Enter</button>
        </form>

        <hr/>
        <h3>Ready to pick a winner?</h3>
        <button onClick={this.onPickWinner}>Pick a winner!</button>
        <hr/>
        <h1>{this.state.message}</h1>
      </div>
    );
  }
}

export default App;
