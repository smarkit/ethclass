pragma solidity ^0.4.7;

contract Inbox {
    string public message;
    
    constructor(string inMessage) public {
        message = inMessage;
    }
    
    function setMessage(string newMessage) public {
        message = newMessage;
    }
}