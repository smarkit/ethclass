const chai = require("chai");
const assert = chai.assert;

const Web3 = require("web3");
const mocha = require("mocha");
const ganache = require("ganache-cli");
const contracts = require("../compile.js");

// create web3 providing using ganache.
const web3 = new Web3(ganache.provider());
const { interface, bytecode } = contracts.get("Inbox");

let accts;
let inboxContract;

// load ganache node and retrieve accounts. 
beforeEach(async() => {
    // get all acounts
    accts = await web3.eth.getAccounts()

    // get a random acct and deploy Inbox contract.
    inboxContract = await (new web3.eth.Contract(JSON.parse(interface))
        .deploy({ data: bytecode, arguments: ["Hi there!"] })
        .send({ from: accts[0], gas: "1000000" }))
});

// test basic message setup.
describe("Inbox", () => {
    it("Should have deployed Inbox contract", () => {
        assert.isDefined(inboxContract);
        assert.isDefined(inboxContract.options);
        assert.isDefined(inboxContract.options.address);
    });
    it("should be able to get Inbox Message", async() => {
        const message = await inboxContract.methods.message().call();
        assert.isDefined(message);
        assert.notEmpty(message);
        assert.equal(message, "Hi there!");
    });

    it("should be able to set Inbox Message", async() => {
        const address = await inboxContract.methods.setMessage("Lego!").send({ from: accts[1], gas: "100000" });
        assert.isDefined(address);
        assert.notEmpty(address);

        const message = await inboxContract.methods.message().call();
        assert.isDefined(message);
        assert.notEmpty(message);
        assert.equal(message, "Lego!");
    });
});